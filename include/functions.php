<?php 

// Verifica se o usuário está logado
function is_logado($user,$pass){
	global $conn;
	$result = $conn->query("select * from tbl_user where user='{$user}' and pass='{$pass}'");
	if($myrow = $result->fetch_array(MYSQLI_ASSOC)){
		return $myrow["id_user"];
	}
	return false;
}

// Insere um post
function insert_post($title, $content, $slug){
	global $conn;
	if($conn->query("insert into tbl_post(title, content, slug)values('{$title}','{$content}','{$slug}')")===true){
		return true;
	}
	return false;
}

// Edita um post
function update_post($id_post, $array_update=array()){
	global $conn;
	if(!empty($array_update)){
		$straux = "";
		foreach($array_update as $k=>$a){
			$straux.= "{$k}='{$a}',";
		}
		$straux = trim($straux, ',');
		$query = "update tbl_post set {$straux} where id_post={$id_post}";
		if($conn->query($query)===true)
			return true;	
	}
	return false;
}

// Busca no banco de dados um post pelo ID
function get_user_by_id($id_user){
	global $conn;
	$result = $conn->query("select * from tbl_user where id_user={$id_user}");
	if($myrow = $result->fetch_array(MYSQLI_ASSOC)){
		return $myrow;
	}
	return false;
}

// busca no banco de dados todos os posts
function get_all_posts(){
	global $conn;
	$result = $conn->query("select * from tbl_post");
	$posts = array();
	while($myrow = $result->fetch_array(MYSQLI_ASSOC)){
		$posts[] = $myrow;
	}
	if(!empty($posts))
		return $posts;
	return false;
}

// deleta um post do banco de dados.
function delete_post($id_post){
	global $conn;
	if($conn->query("delete from tbl_post where id_post={$id_post}")===true){
		return true;
	}
	return false;
}

// busca no banco de dados um post pelo SLUG.
function get_post_by_slug($slug){
	global $conn;
	$result = $conn->query("select * from tbl_post where slug='{$slug}'");
	if($myrow = $result->fetch_array(MYSQLI_ASSOC)){
		return $myrow;
	}
	return false;
}

 ?>