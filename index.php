<?php 
require_once("include/db.php");
require_once("include/functions.php");

if(isset($_REQUEST["section"])){

	session_start();
	$session_name = base64_encode("id_user");

	switch ($_REQUEST["section"]) {


		// Seção: Login
		// Descrição: recebe via GET ou POST o usuário e senha, procura no banco de dados. Caso encontre, gera uma session e retorna true. Caso contrário retorna false com o código do erro e uma mensagem.
		case 'login': // ok
			$_user = isset($_REQUEST["user"]) ? addslashes($_REQUEST["user"]) : false;
			$_pass = isset($_REQUEST["pass"]) ? addslashes($_REQUEST["pass"]) : false;

			if($_user && $_pass){
				if(!isset($_SESSION[$session_name])){
					if($id_user = is_logado($_user,$_pass)){
						$_SESSION[$session_name] = base64_encode($id_user);
						die(json_encode(array(
							"success" => true
						)));
					}else{
						die(json_encode(array(
							"success" => false,
							"error_code" => 447,
							"error_message" => "Invalid username or password"
						)));
					}
				}else{
					die(json_encode(array(
						"success" => false,
						"error_code" => 446,
						"error_message" => "You are already logged in"
					)));
				}
			}else{
				die(json_encode(array(
					"success" => false,
					"error_code" => 448,
					"error_message" => "Missing user or password"
				)));
			}
			break;

		// Seção: Logout
		// Descrição: verifica se existe session existente, se existir, dele delete e retorna true. Caso contrário retorna false com o código do erro e uma mensagem.
		case 'logout': // ok
				if(isset($_SESSION[$session_name])){
					unset($_SESSION[$session_name]);
					die(json_encode(array(
						"success" => true
					)));
				}else{
					die(json_encode(array(
						"success" => false,
						"error_code" => 445,
						"error_message" => "You must be logged in to logout"
					)));
				}
			break;

		// Seção: List Posts
		// Descrição: lista os posts que estão cadastrados no banco de dados.
		case 'list-posts': // ok
			if(isset($_SESSION[$session_name])){
				if($user = get_user_by_id(addslashes(base64_decode($_SESSION[$session_name])))){
					if($posts = get_all_posts()){
						die(json_encode(array(
							"success" => true,
							"posts" => $posts
						)));
					}else{
						die(json_encode(array(
							"success" => false,
							"error_code" => 440,
							"error_message" => "There is no post in the database."
						)));
					}					
				}
			}else{
				die(json_encode(array(
					"success" => false,
					"error_code" => 444,
					"error_message" => "You must be logged"
				)));
			}
			break;

		// Seção: Add Post
		// Descrição: recebe via post ou get os dados para a inserção de um post novo e se tudo estiver ok, ele tenta incluir no banco de dados.
		case 'add-post': // ok
			if(isset($_SESSION[$session_name])){
				if($user = get_user_by_id(addslashes(base64_decode($_SESSION[$session_name])))){
			
					$_title = isset($_REQUEST["title"]) ? addslashes($_REQUEST["title"]) : false;
					$_content = isset($_REQUEST["content"]) ? addslashes($_REQUEST["content"]) : false;
					$_slug = isset($_REQUEST["slug"]) ? addslashes($_REQUEST["slug"]) : false;

					if($_title && $_content && $_slug){
						if(insert_post($_title, $_content, $_slug)){
							die(json_encode(array(
								"success" => true
							)));
						}else{
							die(json_encode(array(
								"success" => false,
								"error_code" => 442,
								"error_message" => "There was a problem trying to include it. Try again."
							)));
						}
					}else{
						die(json_encode(array(
							"success" => false,
							"error_code" => 443,
							"error_message" => "Missing title, content or slug"
						)));
					}
				}
			}else{
				die(json_encode(array(
					"success" => false,
					"error_code" => 444,
					"error_message" => "You must be logged"
				)));
			}
			break;

		// Seção: Edit Post
		// Descrição: recebe via post ou get os dados para a update de um post novo e se tudo estiver ok, ele edita o post no banco
		case 'edit-post': // ok
			if(isset($_SESSION[$session_name])){
				if($user = get_user_by_id(addslashes(base64_decode($_SESSION[$session_name])))){

					$update_array = array();
					
					if(isset($_REQUEST["title"])){
						$update_array["title"] = addslashes($_REQUEST["title"]);
					}
					if(isset($_REQUEST["content"])){
						$update_array["content"] = addslashes($_REQUEST["content"]);
					}
					if(isset($_REQUEST["slug"])){
						$update_array["slug"] = addslashes($_REQUEST["slug"]);
					}

					if(update_post(1,$update_array)){
						die(json_encode(array(
							"success" => true
						)));
					}else{
						die(json_encode(array(
							"success" => false,
							"error_code" => 441,
							"error_message" => "There was a problem trying to update it. Try again."
						)));
					}

				}
			}else{
				die(json_encode(array(
					"success" => false,
					"error_code" => 444,
					"error_message" => "You must be logged"
				)));
			}
			break;

		// Seção: Delete Post
		// Descrição: Verifica se existe o id do post no banco de dados, se existir, ele exclui do banco de dados
		case 'delete-post': // ok
			if(isset($_SESSION[$session_name])){
				if($user = get_user_by_id(addslashes(base64_decode($_SESSION[$session_name])))){
					$_id_post = isset($_REQUEST["id_post"]) ? addslashes($_REQUEST["id_post"]) : false;
					if($_id_post){
						if(delete_post($_id_post)){
							die(json_encode(array(
								"success" => true
							)));
						}else{
							die(json_encode(array(
								"success" => false,
								"error_code" => 438,
								"error_message" => "Wrong id_post"
							)));
						}
					}else{
						die(json_encode(array(
							"success" => false,
							"error_code" => 439,
							"error_message" => "Missing id_post"
						)));
					}
				}
			}else{
				die(json_encode(array(
					"success" => false,
					"error_code" => 444,
					"error_message" => "You must be logged"
				)));
			}
			break;

		// Seção: View Post
		// Descrição: Recebe o slug do post e procura no banco de dados se existe um post lá com este slug. Caso não existe ele retorna false com o código do erro.
		case 'view-post':
			if(isset($_SESSION[$session_name])){
				if($user = get_user_by_id(addslashes(base64_decode($_SESSION[$session_name])))){
					$_slug = isset($_REQUEST["slug"]) ? addslashes($_REQUEST["slug"]) : false;
					if($_slug){
						if($post = get_post_by_slug($_slug)){
							die(json_encode(array(
								"success" => true,
								"post" => $post
							)));
						}else{
							die(json_encode(array(
								"success" => false,
								"error_code" => 436,
								"error_message" => "Wrong slug"
							)));
						}
					}else{
						die(json_encode(array(
							"success" => false,
							"error_code" => 437,
							"error_message" => "Missing slug"
						)));
					}
				}
			}else{
				die(json_encode(array(
					"success" => false,
					"error_code" => 444,
					"error_message" => "You must be logged"
				)));
			}
			break;

		default:
			die(json_encode(array(
				"success" => false,
				"error_code" => 449,
				"error_message" => "Wrong endpoint"
			)));
			break;
	}
}

?>